daily-high-level-tests
======================

This package provides high-level tests using headless browser.

It requires:

* Scriptable headless browser: PhantomJS (alone, but without ES6 support) or SlimerJS + Firefox
* CasperJS to extend its capabilities for automated web page testing.
* Enough of RAM, otherwise Xvfb service and casperjs are unexpectedly exiting

The requirements should be installed in the following order.


Setup virtual display (if needed) for Firefox
=============================================

    apt-get install xvfb

Insert this code to `~/.profile`:

    export DISPLAY=:0

Apply by `source ~/.profile`

Create `/etc/init.d/xvfb` script:

    #!/bin/sh

    ### BEGIN INIT INFO
    # Provides:       xvfb
    # Required-Start:
    # Required-Stop:
    # Default-Start:  2 3 4 5
    # Default-Stop:   0 1 6
    ### END INIT INFO

    XVFB=/usr/bin/Xvfb
    XVFBARGS=":0"
    PIDFILE=/var/run/xvfb.pid
    case "$1" in
      start)
        start-stop-daemon --start --quiet --pidfile $PIDFILE --make-pidfile --background --exec $XVFB -- $XVFBARGS
        ;;
      stop)
        start-stop-daemon --stop --quiet --pidfile $PIDFILE
        ;;
      restart)
        $0 stop
        $0 start
        ;;
    esac
    exit 0

Then run the service and enable it at boot:

    chmod +x /etc/init.d/xvfb
    rm -rf /tmp/.X11-unix
    update-rc.d xvfb defaults
    update-rc.d xvfb enable
    service xvfb start

Run `service xvfb status` to check that it is loaded, enabled, active and running.

You can also use virtual display without the init.d script (e.g. if you need more than one display):

    pkill -f 'Xvfb :2' || true
    rm -f /tmp/.X11-unix/X2
    rm -f /tmp/.X2-lock
    Xvfb :2 &

Monitor xvfb service
--------------------

Install monit to automatically start xvfb if it exits:

    apt-get install monit

Edit `/etc/monit/monitrc` and uncomment lines:

    set httpd port 2812
       use address localhost
       allow localhost
       allow admin:monit

Install check and reload monit configuration:

    ln -s $(pwd)/monit/xvfb /etc/monit/conf.d/xvfb
    service monit restart
    monit status xvfb


Firefox
=======

Warning: currently there is no option to [allow insecure localhost certificates for headless testing](https://bugzilla.mozilla.org/show_bug.cgi?id=1396221).

Download compatible Firefox from [mozilla.org](https://ftp.mozilla.org/pub/firefox/releases/)

E.g. `wget https://ftp.mozilla.org/pub/firefox/releases/58.0.2/linux-x86_64/en-GB/firefox-58.0.2.tar.bz2`

Decompress it with `bzip2 -d firefox-58.0.2.tar.bz2` && `tar xvf firefox-58.0.2.tar`

Then move the directory: `mv -f firefox /opt/firefox-58`

Remove binary of the latest Firefox: `sudo mv /usr/bin/firefox /usr/bin/firefox-bak`

Create a symlink: `sudo ln -s /opt/firefox-58/firefox /usr/bin/firefox`

Run `firefox --version`. Install this package if it is missing: `apt-get install libdbus-glib-1-2`


Disable automatic updates
-------------------------

SlimerJS will prevent automatic updates. If you want to prevent them completely:

* Disconnect from internet.
* Run firefox, open `about:config` in the address bar, search for the `app.update.auto` setting. Double-click it.
* Remove the `updates` directory.


NodeJS
=======

Install `nodejs` from [nodejs.org](https://nodejs.org/en/download/package-manager/)


SlimerJS
========

On Debian: download from [slimerjs.org](https://slimerjs.org/download.html), unpack and create symlink:

    wget https://download.slimerjs.org/releases/1.0.0/slimerjs-1.0.0.tar.bz2
    bzip2 -d slimerjs-1.0.0.tar.bz2 && tar xvf slimerjs-1.0.0.tar
    sudo mv slimerjs-1.0.0 /opt
    sudo ln -s /opt/slimerjs-1.0.0/slimerjs /usr/bin/slimerjs

Or PC: install with npm (more tests can fail in casperjs self test if installed on Debian):

    cd daily-high-level-tests
    npm install slimerjs
    sudo ln -s /home/loadtest/daily-high-level-tests/node_modules/.bin/slimerjs /usr/bin/slimerjs

You may need to upgrade nodejs if `/usr/bin/nodejs` is missing. See [nodejs.org](https://nodejs.org/en/download/package-manager).

Set path to firefox, run headless (add it to `~/.profile` and apply with `source ~/.profile`):

    export SLIMERJSLAUNCHER=/opt/firefox-58/firefox
    export MOZ_HEADLESS=1

You can run `unset MOZ_HEADLESS` whenever you need to see the browser window on PC.


Browser profile
---------------

For using SSL and customizing browser options, profile must be created.

    su loadtest
    slimerjs --createprofile AllowSSL

It will create directory `~/.innophi/slimerjs/*.AllowSSL/`

Create file `node_modules/slimerjs/src/defaults/preferences/user.js` if you installed slimerjs by npm
or `~/.innophi/slimerjs/*.AllowSSL/user.js` if you downloaded slimerjs.

Add setting `user_pref("javascript.options.strict", false);`


Cache
-----

Limit for cache size can be set when running casperjs, but it may overfill even so. You can set cron to purge it:

    16 2 * * * root find /home/loadtest/.cache/innophi/slimerjs/3ik0at1g.AllowSSL/cache2/trash* -type d -mtime +2 -prune -exec rm -rf '{}' \; >> /dev/null
    22 2 * * * root find /home/loadtest/.cache/innophi/slimerjs/3ik0at1g.AllowSSL/cache2/entries/* -type d -m time +2 -prune -exec rm -rf '{}' \; >> /dev/null

SSL certificates
----------------

Copy the `cert_override.txt` into the browser profile directory.

Steps to create a new security exception from PC:

* Run `firefox`
* Open the secured page
* Add a security exception
* cat `~/.mozilla/firefox/*/cert_override.txt`
* Find row with the entered IP
* Copy the row to `~/.innophi/slimerjs/*.AllowSSL/cert_override.txt`


CasperJS
========

Installation
------------

	sudo npm install -g casperjs # @1.1.4


Configuration
-------------

Edit `config.js`


Run a single or all tests with SlimerJS
---------------------------------------

	casperjs --engine=slimerjs --max-disk-cache-size=200000 -P AllowSSL test tests/test_configmgt_login.js
	casperjs --engine=slimerjs --max-disk-cache-size=200000 -P AllowSSL test tests


Run a single test or all with PhantomJS
---------------------------------------

	casperjs --ignore-ssl-errors=true test tests/test_configmgt_login.js
	casperjs --ignore-ssl-errors=true test tests


Debug
-----

Run CasperJS self test:

    casperjs --engine=slimerjs selftest

Add these options to command line:

    --verbose --log-level=debug

Print to console, image or file:

    this.echo(this.getHTML());
    casper.capture('result.png')
    fs.write('result.html', this.getHTML());


Important notes
---------------

Xvfb service sometimes exits and must be restarted.

Tested page stops responding if there is a javascript error. It can be caused by missing DNS resolver configuration.
Third party url in xivocc `custom.env` file must be resolvable to avoid errors in CC Agent/Manager and UC Assistant.

Function `waitFor()` can't be used with `timeout` parameter in casperjs 1.1.4 - it doesn't work!
Default wait timeout can be changed by setting `casper.options.waitTimeout = 1000`. It should be set back to 5000 before
test end for the case if whole suite is tested with one casperjs instance.
Or `wait([ms])` can be added before a step.

Html loads correctly, but some resources are failing to download. They can be listed:

    casper.on("resource.received", function(resource) { this.echo("Recource received: " + resource.url); });
    casper.on("resource.error", function(resourceError) { this.echo("Failed to load: " + resourceError.url); });


PhantomJS
=========

Installation
------------

	sudo apt install phantomjs

On machine without display this environmental variable and symlink must be set:

    QT_QPA_PLATFORM=offscreen
    ln -s /usr/share/fonts /usr/lib/x86_64-linux-gnu/fonts


Documentation
=============

[CasperJS API](http://docs.casperjs.org/en/latest/modules/index.html)
[SlimerJS API](https://docs.slimerjs.org/current/api/fs.html)

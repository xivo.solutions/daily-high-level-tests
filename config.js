var custom_config = require('./custom_demo_config.js')

// DAILY
// var xivoHost = "192.168.225.107";
// var xucHost = "192.168.225.131";
// var kibanaPermalinkId = ""

// DEMO
//var xivoHost = "192.168.226.111";
var xivoHost = "xivo-demo.xivo.solutions";
//var xucHost = "192.168.226.112";
var xucHost = "xivocc-demo.xivo.solutions";
var kibanaPermalinkId = "fc6f1c00-a2f8-11e9-a737-f3b2e94b867f"

// DEV
// var xivoHost = "192.168.227.10";
// var xucHost = "192.168.227.100";
// var xucHost = "xivocc.dev.avencall.com";
// var kibanaPermalinkId = "fc6f1c00-a2f8-11e9-a737-f3b2e94b867f"

// LOAD
// var xivoHost = "192.168.228.10";
// var xucHost = "192.168.228.100";
// var kibanaPermalinkId = "fc6f1c00-a2f8-11e9-a737-f3b2e94b867f"

// LOCAL
// var xivoHost = "192.168.56.2";
// var xucHost = "192.168.56.3";
// var kibanaPermalinkId = ""

var login = custom_config.username;
var password = custom_config.password;
var number = custom_config.number;
var kibanaLogin = custom_config.kibanaUsername;
var kibanaPassword = custom_config.kibanaPassword;

//var configUrl = "http://" + xivoHost + ":9100/configmgt/login";
var configUrl = "http://" + xivoHost + "/configmgt/login";
//var ccmanagerUrl = "http://" + xucHost + ":8070/ccmanager";
var ccmanagerUrl = "https://" + xucHost + "/ccmanager";
//var ccagentUrl = "http://" + xucHost + ":8070/ccagent";
var ccagentUrl = "https://" + xucHost+ "/ccagent";
//var ucassistantUrl = "http://" + xucHost + ":8070";
var ucassistantUrl = "http://" + xucHost;
//var ucassistantUrl = "https://" + xucHost;
var ucassistantSSLUrl = "https://" + xucHost;
var fingerboardUrl = "https://" + xucHost + "/fingerboard";
var recordingUrl = "https://" + xucHost + "/recording/";
//var spagobiUrl = "http://" + xucHost + ":9500/SpagoBI";
var spagobiUrl = "http://" + xucHost + "/SpagoBI";
var kibanaUrl = "https://" + xucHost + "/kibana";
var xucPageUrl = "http://" + xucHost + ""
var samplePageUrl = "http://" + xucHost + ":8090/sample"
// var webrtcSamplePageUrl = "https://" + xucHost + ":8443/sample"
var webrtcSamplePageUrl = "https://" + xucHost + "/sample"

module.exports = {
    configUrl: configUrl,
    ccmanagerUrl: ccmanagerUrl,
    ccagentUrl: ccagentUrl,
    ucassistantUrl: ucassistantUrl,
    ucassistantSSLUrl: ucassistantSSLUrl,
    fingerboardUrl: fingerboardUrl,
    recordingUrl: recordingUrl,
    spagobiUrl: spagobiUrl,
    kibanaUrl: kibanaUrl,
    kibanaPermalinkId: kibanaPermalinkId,
    xucPageUrl: xucPageUrl,
    samplePageUrl: samplePageUrl,
    webrtcSamplePageUrl: webrtcSamplePageUrl,
    login: login,
    password: password,
    number: number,
    kibanaLogin: kibanaLogin,
    kibanaPassword: kibanaPassword,
}

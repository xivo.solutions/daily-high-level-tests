
var config = require("../config.js");

casper.test.begin('Test login to ConfigMgt', function suite(test) {

    casper.start(config.configUrl, function() {
        this.echo("Opening " + config.configUrl, "INFO");
        test.assertExists("#loginbutton");
        test.assertDoesntExist(".container-fluid");
    });

    casper.waitForSelector(".form", function(){
        test.assertExists(".form");
    });

    casper.then(function() {
        this.echo("Logging user avencall/superpass", "INFO");
        this.fill(".form", {
            "login": "avencall",
            "mdp": "superpass"
        }, true);
    });

    casper.waitForSelector(".container-fluid", function(){
        test.assertExists(".container-fluid");
    });

    casper.run(function() {
        test.done();
    });
})

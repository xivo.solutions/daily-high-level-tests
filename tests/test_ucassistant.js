
var config = require("../config.js")

casper.test.begin("Test login to UC Assistant", function suite(test) {

    casper.start(config.ucassistantUrl, function() {
        this.echo("Opening " + config.ucassistantUrl, "INFO");
        test.assertTitle("XiVO UC Assistant");
    });

    casper.waitForSelector(".loginForm", function(){
        test.assertExists(".loginForm");
        test.assertDoesntExist('.main-app');
    });

    casper.then(function() {
        this.echo("Logging user acd999999/1111", "INFO");
        this.fill(".loginForm", {
            "username": "acd999999",
            "password": "1111"
        }, true);
    });

    casper.wait(5000, function() {
      this.echo("I've waited for 5 seconds.");
    });

    casper.waitForText("Invalid username or password", function(){
        test.assertTextExists("Invalid username or password", "Find text matching: Invalid username or password")
    });

    casper.then(function() {
        this.echo("Logging user " + config.login + "/1111/", "INFO");
        this.fill(".loginForm", {
            "username": config.login,
            "password": "1111"
        }, true);
    });

    casper.waitForText("Invalid username or password", function(){
        test.assertTextExists("Invalid username or password", "Find text matching: Invalid username or password")
    });

    casper.then(function() {
        this.echo("Logging user " + config.login + "/" + config.password, "INFO");
        this.fill(".loginForm", {
            "username": config.login,
            "password": config.password
        }, true);
    });

    casper.wait(5000, function() {
      this.echo("I've waited for 5 seconds.");
    });

    casper.waitForSelector(".main-app", function(){
        test.assertDoesntExist('.loginForm');
        test.assertExists(".main-app");
    });

    casper.run(function() {
        test.done();
    });
})


var config = require("../config.js");

casper.test.begin('Test Nginx', function suite(test) {

    casper.start(config.samplePageUrl, function() {
        this.echo("Opening " + config.samplePageUrl, "INFO");
        test.assertTitle("XiVOxc API Sample Page");
    });

    casper.waitForResource("sample.js", function() {
        this.echo('sample.js has been loaded');
    });

    casper.thenOpen(config.xucPageUrl, function() {
        this.echo("Opening " + config.xucPageUrl, "INFO");
        test.assertTitle("Xuc");
    });

    casper.thenOpen(config.webrtcSamplePageUrl, function() {
        this.echo("Opening " + config.webrtcSamplePageUrl, "INFO");
        test.assertTitle("XiVOxc API Sample Page");
    });

    casper.run(function() {
        test.done();
    });
})


var config = require("../config.js");

var dashboardUrl = config.kibanaUrl + "/app/kibana#/dashboard/" + config.kibanaPermalinkId + "?_g=()"

casper.test.begin('Test Kibana', function suite(test) {

    casper.start(dashboardUrl, function() {
        this.echo("Opening " + dashboardUrl, "INFO");
    });

    casper.wait(10000, function() {
      this.echo("I've waited for 10 seconds.");
    });

    // casper.wait(10000, function() {
    //   this.echo("I've waited for 10 seconds.");
    // });

    casper.waitForText("CC - QUEUES", function(){
        test.assertTextExists("CC - QUEUES", "Find text matching: " + "CC - QUEUES - Kibana");
    });

    casper.run(function() {
        test.done();
    });

})

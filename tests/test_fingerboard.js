
var selectXPath = require('casper').selectXPath;
var config = require("../config.js")

casper.test.begin("Test Fingerboard", function suite(test) {

    casper.start(config.fingerboardUrl, function() {
        this.echo("Opening " + config.fingerboardUrl, "INFO");
        test.assertTitle("Fingerboard XiVO CC");
    });

    casper.then(function() {
        test.assertEquals(this.fetchText(selectXPath("//div[contains(text(),'Bandeau agent')]")), "Bandeau agent", "Click button Bandeau agent");
        this.click(selectXPath("//div[contains(text(),'Bandeau agent')]"));
    });

    casper.wait(5000, function() {
      this.echo("I've waited for 5 seconds.");
    });

    casper.waitForPopup({title:'XiVO CC Agent'}, function() {
        this.withPopup(0, function() {
            this.test.assertTitle('XiVO CC Agent');

            return this.evaluate(function() {
                window.close();
            });
        });
    });

    casper.waitForSelector(selectXPath("//div[contains(text(),'CC manager')]"), function() {
        test.assertEquals(this.fetchText(selectXPath("//div[contains(text(),'CC manager')]")), "CC manager", "Click button CC manager");
        this.click(selectXPath("//div[contains(text(),'CC manager')]"));
    });

    casper.waitForPopup({title:'XiVO CC Manager'}, function() {
        this.withPopup(0, function() {
            this.test.assertTitle('XiVO CC Manager');

            return this.evaluate(function() {
                window.close();
            });
        });
    });

    casper.waitForSelector(selectXPath("//div[contains(text(),'Enregistrements')]"), function() {
        test.assertEquals(this.fetchText(selectXPath("//div[contains(text(),'Enregistrements')]")), "Enregistrements", "Click button Enregistrements");
        this.click(selectXPath("//div[contains(text(),'Enregistrements')]"));
    });

    casper.waitForPopup({title:'Login'}, function() {
        this.withPopup(0, function() {
            test.assertTextExists("Liste des enregistrements", "Find text matching: Liste des enregistrements")

            return this.evaluate(function() {
                window.close();
            });
        });
    });

    casper.waitForSelector(selectXPath("//div[contains(text(),'Statistiques')]"), function() {
        test.assertEquals(this.fetchText(selectXPath("//div[contains(text(),'Statistiques')]")), "Statistiques", "Click button Statistiques");
        this.click(selectXPath("//div[contains(text(),'Statistiques')]"));
    });

    casper.waitForPopup({title:'SpagoBI'}, function() {
        this.withPopup(0, function() {
            this.test.assertTitle("SpagoBI");

            return this.evaluate(function() {
                window.close();
            });
        });
    });

    casper.waitForSelector(selectXPath("//div[contains(text(),'Monitoring')]"), function() {
        test.assertEquals(this.fetchText(selectXPath("//div[contains(text(),'Monitoring')]")), "Monitoring", "Click button Monitoring");
        this.click(selectXPath("//div[contains(text(),'Monitoring')]"));
    });

    casper.wait(5000, function() {
      this.echo("I've waited for 5 seconds.");
    });

    casper.waitForPopup({title:'Home - Elastic'}, function() {
        this.withPopup(0, function() {
            this.test.assertTitleMatch(/Elastic/, 'Page title contains: "Elastic"');

            return this.evaluate(function() {
                window.close();
            });
        });
    });

    casper.run(function() {
        casper.options.waitTimeout = 5000;
        test.done();
    });
})

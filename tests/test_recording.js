
var config = require("../config.js");

var login = "enzo.piccolino"
var password = "enzo"

casper.test.begin('Test login to Recording', function suite(test) {

    casper.start(config.recordingUrl, function() {
        this.echo("Opening " + config.recordingUrl, "INFO");
        test.assertTitle("Login");
    });

    casper.then(function() {
        this.echo(this.evaluate(function() {
            var prom = new Promise(function(resolve, reject) { });
            return "Creating a Promise to test the browser: " + prom;
        }));
    });

    casper.waitForSelector(".form", function(){
        test.assertExists(".form");
    });

    casper.then(function() {
        this.echo("Logging user " + login + "/" + password, "INFO");
        this.fill(".form", {
            "login": config.login,
            "mdp": config.password
        }, true);
    });

    casper.wait(5000, function() {
      this.echo("I've waited for 5 seconds.");
    });

    casper.waitFor(function check() {
        return this.evaluate(function() {
            return (document.title != "Login");
        });
    });

    casper.then(function() {
        test.assertTitle("Liste des enregistrements");
    });

    casper.wait(5000, function() {
      this.echo("I've waited for 5 seconds.");
    });

    casper.waitForSelector('.download', function() {
        test.assertExists(".download");
    });

    casper.waitForSelector('#records', function() {
        var record_name = "record.wav";

        var record = this.evaluate(function() {
           return angular.element("#records").find("tr").next().children().last().children("a").attr("href");
           //return angular.element("#records").find("tr").next().next().next().next().children().last().children("a").attr("href");
        });

        if (fs.exists(record_name)) {
            fs.remove(record_name);
        }

        //this.echo("SKIPPING DOWNLOAD - SOMETIMES CASPER WAITS HERE INFINITELY");
        test.assert(! fs.exists(record_name), "File " + record_name + " doesn't exist");

        this.echo("Downloading " + config.recordingUrl + "/" + record + " to " + fs.workingDirectory);
        this.download(config.recordingUrl + "/" + record, record_name);

        test.assert(fs.exists(record_name), "File " + record_name + " exists");

        var size = fs.size(record_name);
        test.assert(size > 10, "File size is over 10 bytes: " + size);
    });

    casper.run(function() {
        test.done();
    });
})

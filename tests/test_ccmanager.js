
var config = require("../config.js")

casper.test.begin("Test login to CC Manager", function suite(test) {

    casper.start(config.ccmanagerUrl, function() {
        this.echo("Opening " + config.ccmanagerUrl, "INFO");
        test.assertTitle("XiVO CC Manager");
    });

    casper.waitForSelector(".loginForm", function(){
        test.assertExists(".loginForm");
        test.assertDoesntExist('.sidenav');
    });

    casper.then(function() {
        this.echo("Logging user acd999999/1111", "INFO");
        this.fill(".loginForm", {
            "username": "acd999999",
            "password": "1111"
        }, true);
    });

    casper.waitForText("User not found", function(){
        test.assertTextExists("User not found", "Find text matching: User not found")
    });

    casper.then(function() {
        this.echo("Logging user " + config.login + "/1111/", "INFO");
        this.fill(".loginForm", {
            "username": config.login,
            "password": "1111"
        }, true);
    });

    casper.waitForText("Invalid password", function(){
        test.assertTextExists("Invalid password", "Find text matching: Invalid password")
    });

    casper.then(function() {
        this.echo("Logging user " + config.login + "/" + config.password, "INFO");
        this.fill(".loginForm", {
            "username": config.login,
            "password": config.password
        }, true);
    });

    casper.wait(5000, function() {
      this.echo("I've waited for 5 seconds.");
    });

    casper.waitForSelector(".sidenav", function(){
        test.assertDoesntExist('.loginForm');
        test.assertExists(".sidenav");
    });

    casper.run(function() {
        test.done();
    });
})

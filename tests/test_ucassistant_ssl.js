
var config = require("../config.js")

casper.test.begin("Test open UC Assistant on https", function suite(test) {

    casper.start(config.ucassistantSSLUrl, function() {
        this.echo("Opening " + config.ucassistantSSLUrl, "INFO");
        test.assertTitle("XiVO UC Assistant");
    });

    casper.run(function() {
        test.done();
    });
})

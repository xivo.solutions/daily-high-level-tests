
var config = require("../config.js");

casper.test.begin('Test login to SpagoBI', function suite(test) {

    casper.start(config.spagobiUrl, function() {
        this.echo("Opening " + config.spagobiUrl, "INFO");
        test.assertTitle("SpagoBI");
    });

    casper.waitForSelector("#formId", function(){
        test.assertExists("#formId");
    });

    casper.then(function() {
        this.echo("Logging user biadmin/biadmin", "INFO");
        this.fill("#formId", {
            "userID": config.kibanaLogin,
            "password": config.kibanaPassword
        }, true);
    });

    casper.waitForResource("welcome.png", function() {
        this.echo('welcome.png has been loaded');
    });

    casper.run(function() {
        test.done();
    });
})

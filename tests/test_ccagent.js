
var config = require("../config.js")

casper.test.begin("Test login to CC Agent", function suite(test) {

    casper.start(config.ccagentUrl, function () {
        this.echo("Opening " + config.ccagentUrl, "INFO");
        test.assertTitle("XiVO CC Agent");
    });

    casper.waitForSelector(".loginForm", function () {
        test.assertExists(".loginForm");
        test.assertDoesntExist('.call-control');
    });

    casper.then(function () {
        this.echo("Logging user acd999999/1111/" + config.number, "INFO");
        this.fill(".loginForm", {
            "username": "acd999999",
            "password": "1111",
            "phoneNumber": config.number
        }, true);
    });

    casper.wait(5000, function () {
        this.echo("I've waited for 5 seconds.");
    });

    casper.waitForText("Invalid username or password", function () {
        test.assertTextExists("Invalid username or password", "Find text matching: Invalid username or password.")
    });

    casper.then(function () {
        this.echo("Logging user " + config.login + "/1111/" + config.number, "INFO");
        this.fill(".loginForm", {
            "username": config.login,
            "password": "1111",
            "phoneNumber": config.number
        }, true);
    });

    casper.waitForText("Invalid username or password", function () {
        test.assertTextExists("Invalid username or password", "Find text matching: Invalid username or password")
    });

    casper.then(function () {
        this.echo("Logging user " + config.login + "/" + config.password + "/\"\"", "INFO");
        this.fill(".loginForm", {
            "username": config.login,
            "password": config.password,
            "phoneNumber": ""
        }, true);
    });

    casper.waitForText("An Agent Phone number is required", function () {
        test.assertTextExists("An Agent Phone number is required", "Find text matching: An Agent Phone number is required")
    });

    casper.then(function () {
        this.echo("Logging user " + config.login + "/" + config.password + "/" + config.number, "INFO");
        this.fill(".loginForm", {
            "username": config.login,
            "password": config.password,
            "phoneNumber": config.number
        }, true);
    });

    casper.wait(5000, function () {
        this.echo("I've waited for 5 seconds.");
    });

    casper.waitForSelector(".call-control", function () {
        test.assertDoesntExist('.loginForm');
        test.assertExists(".call-control");
    });

    casper.run(function () {
        test.done();
    });
})
